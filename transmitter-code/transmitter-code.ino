/****** TRANSMITTER CODE ******/
#include <SPI.h>
#include <RF24.h> 
#include <dht.h>

// SETTING UP RADIO AND SENSOR
RF24 radio (7, 8);
dht sensor;
#define DHT_PIN 2 

const uint64_t node01 = 0xE8E8F0F0A1LL;
const uint64_t node02 = 0xE8E8F0F0A2LL;
const uint64_t node03 = 0xE8E8F0F0A3LL;
const uint64_t node04 = 0xE8E8F0F0A4LL;
const uint64_t node05 = 0xE8E8F0F0A5LL;
const uint64_t node06 = 0xE8E8F0F0A6LL;

/****** THINGS TO CHANGE START ***/
// Change to your groups id: node01-05
uint64_t setPipeToSend = node01;
// Transmit delay in milliseconds
int radioDelay = 1000; 
/****** THINGS TO CHANGE END ***/

// PAYLOAD
struct dataPayload {
  int temp;
  int humid;
} sensordata;

/****** RUNS ONCE ******/
void setup() {
  // Use the serial monitor
  Serial.begin(9600);
  delay(1000);
  // Start up the physical nRF24L01 Radio
  radio.begin();
  radio.setChannel(108);
  radio.setDataRate(RF24_250KBPS);
  radio.openWritingPipe(setPipeToSend);
  delay(1000);
}

/****** RUNS CONSTANTLY ******/
void loop() {
  // Read sensor values
  int chk = sensor.read11(DHT_PIN);
  sensordata.temp = sensor.temperature;
  sensordata.humid = sensor.humidity;
  // Transmit the data over radio
  radio.write( &sensordata, sizeof(sensordata) ); 
  // Print out sensor data to serial monitor too
  Serial.print("Temperatur = ");
  Serial.print(sensordata.temp);
  Serial.print(", Fuktighet = ");
  Serial.println(sensordata.humid);
  // Transmit delay
  delay(radioDelay);
}
