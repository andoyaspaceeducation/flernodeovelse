/****** RECEIVER CODE ******/
#include <SPI.h>
#include <RF24.h> 

// SETTING UP RADIO
RF24 radio (7, 8);

// PAYLOAD
struct dataPayload {
  int temp;
  int humid;
} sensordata;

uint8_t nodeNumber;
char buffer [20];
int n;

/****** RUNS ONCE ******/
void setup() {
  Serial.begin(9600);
  delay(1000);
  // Setting unique address for each node that can transmit data
  const uint64_t node01 = 0xE8E8F0F0A1LL;
  const uint64_t node02 = 0xA2LL;
  const uint64_t node03 = 0xA3LL;
  const uint64_t node04 = 0xA4LL;
  const uint64_t node05 = 0xA5LL;
  // Initialize the radio receiver
  radio.begin();
  radio.setChannel(108); 
  radio.setDataRate(RF24_250KBPS);
  radio.openReadingPipe(1, node01);  
  radio.openReadingPipe(2, node02);
  radio.openReadingPipe(3, node03);
  radio.openReadingPipe(4, node04);
  radio.openReadingPipe(5, node05);
  radio.startListening();
}

/****** RUNS CONSTANTLY ******/
void loop() {
  // Check for incoming data from transmitter node
  if (radio.available(&nodeNumber)) { 
    // While there is data ready
    while (radio.available(&nodeNumber)) { 
      // Get the data payload
      radio.read(&sensordata, sizeof(sensordata) ); 
    }
    // Print out received paylod to serial monitor
    n = sprintf (buffer, "%d,%d,%d", nodeNumber, sensordata.temp, sensordata.humid);
    Serial.println(buffer);
  }
}
